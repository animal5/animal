/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.animal;

/**
 *
 * @author acer
 */
public class TestAnimal {
    public static void main(String[] args) {
      Human h1 = new Human("Nan ");
      h1.eat();
      h1.walk();
      h1.run();
      h1.speak();
      h1.sleep(); 
      System.out.println("h1 is animal ? "+(h1 instanceof Animal));
      System.out.println("h1 is land animal ? "+(h1 instanceof LandAnimal));
      Cat c1 = new Cat("Yu ");
      c1.eat();
      c1.run();
      c1.walk();
      c1.sleep();
      c1.speak();
      System.out.println("c1 is animal ? "+(c1 instanceof Animal));
      System.out.println("c1 is land animal ? "+(c1 instanceof LandAnimal));
      Dog d1 = new Dog("Tae ");
      d1.eat();
      d1.run();
      d1.walk();
      d1.sleep();
      d1.speak();
      System.out.println("d1 is animal ? "+(d1 instanceof Animal));
      System.out.println("d1 is land animal ? "+(d1 instanceof LandAnimal));
      Crocodile cr1 = new Crocodile("jae ");
      cr1.eat();
      cr1.walk();
      cr1.sleep();
      cr1.crawl();
      System.out.println("cr1 is animal ? "+(cr1 instanceof Animal));
      System.out.println("cr1 is Reptile ? "+(cr1 instanceof Reptile));
      Snake s1 = new Snake("hae ");
      s1.eat();
      s1.sleep();
      s1.crawl();
      System.out.println("s1 is animal ? "+(s1 instanceof Animal));
      System.out.println("s1 is Reptile ? "+(s1 instanceof Reptile));
      Fish f1 = new Fish("hae ");
      f1.eat();
      f1.sleep();
      f1.swim();
      System.out.println("f1 is animal ? "+(f1 instanceof Animal));
      System.out.println("f1 is AquaticAnimal ? "+(f1 instanceof AquaticAnimal));
      Crab cra1 = new Crab("smile ");
      cra1.eat();
      cra1.sleep();
      cra1.crawl();
      System.out.println("cra1 is animal ? "+(cra1 instanceof Animal));
      System.out.println("cra1 is AquaticAnimal ? "+(cra1 instanceof AquaticAnimal));
      Bat b1 = new Bat("lim ");
      b1.eat();
      b1.sleep();
      b1.fly();
      b1.speak();
      System.out.println("b1 is animal ? "+(b1 instanceof Animal));
      System.out.println("b1 is Poulty ? "+(b1 instanceof Poulty));
      Bird bi1 = new Bird("bi ");
      bi1.eat();
      bi1.sleep();
      bi1.fly();
      bi1.speak();
      System.out.println("bi1 is animal ? "+(bi1 instanceof Animal));
      System.out.println("bi1 is Poulty ? "+(bi1 instanceof Poulty));
    }
            
            
}
