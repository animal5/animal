/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.animal;

/**
 *
 * @author acer
 */
public class Dog extends LandAnimal{
    public String name;
    
    public Dog(String name) {
        super("Dog", 4);
        this.name = name;
    }

    public Dog(String name,int numOfLeg) {
        super(name, numOfLeg);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("Dog: "+ name + "run");
    }

    @Override
    public void eat() {
        System.out.println("Dog: "+ name + "eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog: "+ name + "walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog: "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: "+ name + "sleep");
    }
    
    
    
}
